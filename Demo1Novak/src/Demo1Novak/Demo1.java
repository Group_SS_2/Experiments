package Demo1Novak;

import java.util.Scanner;

public class Demo1 {

	
	public static void main(String args[]){
		int number1 = 0;
		int number2 = 0;
		String function = "";
		int answer = 0;
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Please enter a number");
		number1 = scan.nextInt();
		
		System.out.println("Please enter a second number");
		number2 = scan.nextInt();
		
		System.out.println("Please enter a math function");
		function = scan.next();
		
		if(function.equals("/"))
		answer = number1/number2;
		
		if(function.equals("*"))
			answer = number1 * number2;
		
		if(function.equals("+"))
			answer = number1 + number2;
		
		if(function.equals("-"))
		answer = number1 - number2;
		
		System.out.println(answer);
	}
}
